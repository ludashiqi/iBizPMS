import TaskUIServiceBase from './task-ui-service-base';

/**
 * 任务UI服务对象
 *
 * @export
 * @class TaskUIService
 */
export default class TaskUIService extends TaskUIServiceBase {

    /**
     * Creates an instance of  TaskUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  TaskUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}