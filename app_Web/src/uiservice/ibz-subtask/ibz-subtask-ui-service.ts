import IBZ_SUBTASKUIServiceBase from './ibz-subtask-ui-service-base';

/**
 * 任务UI服务对象
 *
 * @export
 * @class IBZ_SUBTASKUIService
 */
export default class IBZ_SUBTASKUIService extends IBZ_SUBTASKUIServiceBase {

    /**
     * Creates an instance of  IBZ_SUBTASKUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZ_SUBTASKUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}