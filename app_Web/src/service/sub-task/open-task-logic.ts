import { Http,Util } from '@/utils';
import OpenTaskLogicBase from './open-task-logic-base';

/**
 * 任务开始
 *
 * @export
 * @class OpenTaskLogic
 */
export default class OpenTaskLogic extends OpenTaskLogicBase{

    /**
     * Creates an instance of  OpenTaskLogic
     * 
     * @param {*} [opts={}]
     * @memberof  OpenTaskLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}