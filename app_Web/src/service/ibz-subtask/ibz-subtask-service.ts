import { Http,Util } from '@/utils';
import IBZ_SUBTASKServiceBase from './ibz-subtask-service-base';


/**
 * 任务服务对象
 *
 * @export
 * @class IBZ_SUBTASKService
 * @extends {IBZ_SUBTASKServiceBase}
 */
export default class IBZ_SUBTASKService extends IBZ_SUBTASKServiceBase {

    /**
     * Creates an instance of  IBZ_SUBTASKService.
     * 
     * @param {*} [opts={}]
     * @memberof  IBZ_SUBTASKService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}