export default {
  fields: {
    name: '名称编号',
    builder: '构建者',
    desc: '描述',
    id: 'id',
    deleted: '已删除',
    scmpath: '源代码地址',
    filepath: '下载地址',
    stories: '完成的需求',
    bugs: '解决的Bug',
    date: '打包日期',
    product: '产品',
    branch: '平台/分支',
    project: '所属项目',
  },
};