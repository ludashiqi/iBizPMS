
export default {
  fields: {
    spec: '需求描述	',
    duplicatestory: '重复需求ID',
    verify: '验收标准',
    id: '虚拟主键',
    title: '需求名称',
    version: '版本号',
    story: '需求',
  },
	views: {
		mainview9: {
			caption: "需求描述",
      title: '需求描述',
		},
	},
	main_form: {
		details: {
			rawitem1: "", 
			group1: "storyspec基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "虚拟主键", 
			srfmajortext: "需求名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			spec: "需求描述	", 
			verify: "验收标准", 
			id: "虚拟主键", 
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
			n_version_eq: "版本号(等于(=))", 
		},
		uiactions: {
		},
	},
};