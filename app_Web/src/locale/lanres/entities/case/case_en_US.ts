
export default {
  fields: {
    lastediteddate: '修改日期',
    scripteddate: 'scriptedDate',
    color: '标题颜色',
    path: 'path',
    openeddate: '创建日期',
    lastrunresult: '结果',
    linkcase: '相关用例',
    order: '排序',
    howrun: 'howRun',
    version: '用例版本',
    scriptedby: 'scriptedBy',
    openedby: '由谁创建',
    type: '用例类型',
    status: '用例状态',
    auto: 'auto',
    frequency: 'frequency',
    title: '用例标题',
    lasteditedby: '最后修改者',
    reviewedby: '由谁评审',
    deleted: '已删除',
    revieweddate: '评审时间',
    pri: '优先级',
    stage: '适用阶段',
    scriptlocation: 'scriptLocation',
    lastrundate: '执行时间',
    keywords: '关键词',
    scriptstatus: 'scriptStatus',
    frame: '工具/框架',
    substatus: '子状态',
    id: '用例编号',
    precondition: '前置条件',
    lastrunner: '执行人',
    fromcaseversion: '来源用例版本',
    storyversion: '需求版本',
    fromcaseid: '来源用例',
    branch: '平台/分支',
    frombug: '来源Bug',
    story: '相关需求',
    product: '所属产品',
    lib: '所属库',
    module: '所属模块',
    modulename: '模块名称',
    storyname: '需求名称',
    productname: '产品名称',
  },
	views: {
		editview: {
			caption: "测试用例",
      title: '测试用例',
		},
		maingridview: {
			caption: "测试用例",
      title: '测试用例',
		},
		mainnewview: {
			caption: "功能测试",
      title: '功能测试',
		},
		batchnewgridview: {
			caption: "测试用例",
      title: '测试用例',
		},
		maindashboardview: {
			caption: "功能测试",
      title: '功能测试',
		},
		gridview9: {
			caption: "测试用例",
      title: '测试用例',
		},
		gridview: {
			caption: "测试用例",
      title: '测试用例',
		},
	},
	main_form: {
		details: {
			group1: "case基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "用例编号", 
			srfmajortext: "用例标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			title: "用例标题", 
			id: "用例编号", 
		},
		uiactions: {
		},
	},
	mainnew_form: {
		details: {
			group1: "测试用例基本信息", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "用例编号", 
			srfmajortext: "用例标题", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			product: "所属产品", 
			productname: "产品名称", 
			module: "所属模块", 
			modulename: "模块名称", 
			type: "用例类型", 
			story: "相关需求", 
			storyname: "需求名称", 
			title: "用例标题", 
			pri: "优先级", 
			id: "用例编号", 
		},
		uiactions: {
		},
	},
	maingrid_grid: {
		columns: {
			id: "id",
			pri: "P",
			title: "用例标题",
			type: "用例类型",
			openedby: "创建",
			lastrunner: "执行人",
			lastrundate: "执行时间",
			lastrunresult: "结果",
			status: "状态",
			branch: "B",
			uagridcolumn1: "操作",
		},
		uiactions: {
		},
	},
	batchnew_grid: {
		columns: {
			modulename: "所属模块",
			storyname: "相关需求",
			title: "用例标题",
			type: "用例类型",
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			pri: "P",
			title: "用例标题",
			status: "状态",
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			pri: "P",
			title: "用例标题",
			status: "状态",
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	batchnewgridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "行编辑",
			tip: "行编辑",
		},
		deuiaction2: {
			caption: "新建行",
			tip: "新建行",
		},
		deuiaction3: {
			caption: "保存行",
			tip: "保存行",
		},
	},
	maingridviewtoolbar_toolbar: {
		deuiaction3_batchnew: {
			caption: "批量新建用例",
			tip: "批量新建用例",
		},
		seperator2: {
			caption: "",
			tip: "",
		},
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		deuiaction4: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
	mainnewviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: "New",
			tip: "New",
		},
		deuiaction4: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		seperator1: {
			caption: "",
			tip: "",
		},
		deuiaction2: {
			caption: "刷新",
			tip: "刷新",
		},
	},
};