export default {
  fields: {
    lastediteddate: '修改日期',
    scripteddate: 'scriptedDate',
    color: '标题颜色',
    path: 'path',
    openeddate: '创建日期',
    lastrunresult: '结果',
    linkcase: '相关用例',
    order: '排序',
    howrun: 'howRun',
    version: '用例版本',
    scriptedby: 'scriptedBy',
    openedby: '由谁创建',
    type: '用例类型',
    status: '用例状态',
    auto: 'auto',
    frequency: 'frequency',
    title: '用例标题',
    lasteditedby: '最后修改者',
    reviewedby: '由谁评审',
    deleted: '已删除',
    revieweddate: '评审时间',
    pri: '优先级',
    stage: '适用阶段',
    scriptlocation: 'scriptLocation',
    lastrundate: '执行时间',
    keywords: '关键词',
    scriptstatus: 'scriptStatus',
    frame: '工具/框架',
    substatus: '子状态',
    id: '用例编号',
    precondition: '前置条件',
    lastrunner: '执行人',
    fromcaseversion: '来源用例版本',
    storyversion: '需求版本',
    fromcaseid: '来源用例',
    branch: '平台/分支',
    frombug: '来源Bug',
    story: '相关需求',
    product: '所属产品',
    lib: '所属库',
    module: '所属模块',
  },
	views: {
		editview: {
			caption: '测试用例',
      title: '测试用例',
		},
		gridview9: {
			caption: '测试用例',
      title: '测试用例',
		},
		gridview: {
			caption: '测试用例',
      title: '测试用例',
		},
	},
	main_form: {
		details: {
			group1: 'case基本信息', 
			formpage1: '基本信息', 
			group2: '操作信息', 
			formpage2: '其它', 
			srforikey: '', 
			srfkey: '用例编号', 
			srfmajortext: '用例标题', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			title: '用例标题', 
			id: '用例编号', 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			pri: 'P',
			title: '用例标题',
			status: '状态',
		},
		uiactions: {
		},
	},
	main2_grid: {
		columns: {
			pri: 'P',
			title: '用例标题',
			status: '状态',
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: '保存',
			tip: '保存',
		},
		tbitem4: {
			caption: '保存并新建',
			tip: '保存并新建',
		},
		tbitem5: {
			caption: '保存并关闭',
			tip: '保存并关闭',
		},
		tbitem6: {
			caption: '-',
			tip: '',
		},
		tbitem7: {
			caption: '删除',
			tip: '删除',
		},
		tbitem8: {
			caption: '-',
			tip: '',
		},
		tbitem12: {
			caption: '新建',
			tip: '新建',
		},
		tbitem13: {
			caption: '-',
			tip: '',
		},
		tbitem14: {
			caption: '拷贝',
			tip: '拷贝',
		},
		tbitem16: {
			caption: '-',
			tip: '',
		},
		tbitem23: {
			caption: '第一个记录',
			tip: '第一个记录',
		},
		tbitem24: {
			caption: '上一个记录',
			tip: '上一个记录',
		},
		tbitem25: {
			caption: '下一个记录',
			tip: '下一个记录',
		},
		tbitem26: {
			caption: '最后一个记录',
			tip: '最后一个记录',
		},
		tbitem21: {
			caption: '-',
			tip: '',
		},
		tbitem22: {
			caption: '帮助',
			tip: '帮助',
		},
	},
	gridviewtoolbar_toolbar: {
		deuiaction1: {
			caption: '新建',
			tip: '新建',
		},
		deuiaction4: {
			caption: '删除',
			tip: '删除',
		},
		seperator1: {
			caption: '',
			tip: '',
		},
		deuiaction2: {
			caption: '刷新',
			tip: '刷新',
		},
	},
};