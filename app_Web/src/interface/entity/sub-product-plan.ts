/**
 * 产品计划
 *
 * @export
 * @interface SubProductPlan
 */
export interface SubProductPlan {

    /**
     * 名称
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    title?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    id?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    begin?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    desc?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    end?: any;

    /**
     * 已删除
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    deleted?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    order?: any;

    /**
     * 父计划名称
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    parentname?: any;

    /**
     * 平台/分支
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    branch?: any;

    /**
     * 父计划
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    parent?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof SubProductPlan
     */
    product?: any;
}