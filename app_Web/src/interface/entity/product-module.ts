/**
 * 需求模块
 *
 * @export
 * @interface ProductModule
 */
export interface ProductModule {

    /**
     * path
     *
     * @returns {*}
     * @memberof ProductModule
     */
    path?: any;

    /**
     * 逻辑删除标志
     *
     * @returns {*}
     * @memberof ProductModule
     */
    deleted?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof ProductModule
     */
    name?: any;

    /**
     * branch
     *
     * @returns {*}
     * @memberof ProductModule
     */
    branch?: any;

    /**
     * 短名称
     *
     * @returns {*}
     * @memberof ProductModule
     */
    ibizshort?: any;

    /**
     * order
     *
     * @returns {*}
     * @memberof ProductModule
     */
    order?: any;

    /**
     * grade
     *
     * @returns {*}
     * @memberof ProductModule
     */
    grade?: any;

    /**
     * 类型（story）
     *
     * @returns {*}
     * @memberof ProductModule
     */
    type?: any;

    /**
     * owner
     *
     * @returns {*}
     * @memberof ProductModule
     */
    owner?: any;

    /**
     * 叶子模块
     *
     * @returns {*}
     * @memberof ProductModule
     */
    isleaf?: any;

    /**
     * id
     *
     * @returns {*}
     * @memberof ProductModule
     */
    id?: any;

    /**
     * collector
     *
     * @returns {*}
     * @memberof ProductModule
     */
    collector?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof ProductModule
     */
    root?: any;

    /**
     * id
     *
     * @returns {*}
     * @memberof ProductModule
     */
    parent?: any;
}