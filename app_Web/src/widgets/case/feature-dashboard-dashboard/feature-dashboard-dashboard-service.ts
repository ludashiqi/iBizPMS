import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * FeatureDashboard 部件服务对象
 *
 * @export
 * @class FeatureDashboardService
 */
export default class FeatureDashboardService extends ControlService {
}