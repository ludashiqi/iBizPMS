import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DeptTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class DeptTreeExpViewtreeexpbarService
 */
export default class DeptTreeExpViewtreeexpbarService extends ControlService {
}