/**
 * DeptExp 部件模型
 *
 * @export
 * @class DeptExpModel
 */
export default class DeptExpModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof DeptExpModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'admins',
      },
      {
        name: 'fax',
      },
      {
        name: 'deleted',
      },
      {
        name: 'website',
      },
      {
        name: 'company',
        prop: 'id',
      },
      {
        name: 'zipcode',
      },
      {
        name: 'address',
      },
      {
        name: 'backyard',
      },
      {
        name: 'name',
      },
      {
        name: 'guest',
      },
      {
        name: 'phone',
      },
    ]
  }


}