import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DeptUserTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class DeptUserTreeExpViewtreeexpbarService
 */
export default class DeptUserTreeExpViewtreeexpbarService extends ControlService {
}