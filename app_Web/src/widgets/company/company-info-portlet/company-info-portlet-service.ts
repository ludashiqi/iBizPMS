import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * CompanyInfo 部件服务对象
 *
 * @export
 * @class CompanyInfoService
 */
export default class CompanyInfoService extends ControlService {
}