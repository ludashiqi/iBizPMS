import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MainPanel_EditMode 部件服务对象
 *
 * @export
 * @class MainPanel_EditModeService
 */
export default class MainPanel_EditModeService extends ControlService {
}