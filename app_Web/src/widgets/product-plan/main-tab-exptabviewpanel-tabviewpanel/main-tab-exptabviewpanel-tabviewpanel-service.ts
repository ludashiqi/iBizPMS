import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MainTabExptabviewpanel 部件服务对象
 *
 * @export
 * @class MainTabExptabviewpanelService
 */
export default class MainTabExptabviewpanelService extends ControlService {
}