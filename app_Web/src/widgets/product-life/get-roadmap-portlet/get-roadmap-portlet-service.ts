import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * GetRoadmap 部件服务对象
 *
 * @export
 * @class GetRoadmapService
 */
export default class GetRoadmapService extends ControlService {
}