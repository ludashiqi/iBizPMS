import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * FeatureTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class FeatureTreeExpViewtreeexpbarService
 */
export default class FeatureTreeExpViewtreeexpbarService extends ControlService {
}