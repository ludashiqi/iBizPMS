import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * BurnDown 部件服务对象
 *
 * @export
 * @class BurnDownService
 */
export default class BurnDownService extends ControlService {
}