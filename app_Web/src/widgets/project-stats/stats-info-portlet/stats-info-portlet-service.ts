import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * StatsInfo 部件服务对象
 *
 * @export
 * @class StatsInfoService
 */
export default class StatsInfoService extends ControlService {
}