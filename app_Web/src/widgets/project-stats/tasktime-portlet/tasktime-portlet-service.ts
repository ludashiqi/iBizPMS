import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * TASKTIME 部件服务对象
 *
 * @export
 * @class TASKTIMEService
 */
export default class TASKTIMEService extends ControlService {
}